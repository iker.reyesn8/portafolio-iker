let darkMode = localStorage.getItem('darkMode');

const darkModeToggle = document.getElementById("dark-mode__button");

const i_moon = document.getElementById("buttonDarkTheme");

const enableDarkMode = () => {
    // añadir la clase al body
    document.body.classList.add('dark-theme');
    // guardar el valor en LocalStorage
    localStorage.setItem('darkMode', 'enabled');
    // borrar la clase luminosa para quitar el icono
    i_moon.classList.remove("fa-solid", "fa-lightbulb");
    // añadir la clase oscura para mostrar el icono
    i_moon.classList.add("fa-regular", "fa-lightbulb");
}

const disableDarkMode = () => {
    // borrar la clase al body
    document.body.classList.remove('dark-theme');
    // guardar el valor en LocalStorage
    localStorage.setItem('darkMode', 'null');
    // borrar la clase oscura para quitar el icono
    i_moon.classList.remove("fa-regular", "fa-lightbulb");
    // añadir la clase luminosa para mostrar el icono
    i_moon.classList.add("fa-solid", "fa-lightbulb");
}

// comprobar si antes se tenia activado el modo oscuro
if (darkMode === 'enabled') {
    enableDarkMode();
    // borrar la clase luminosa para quitar el icono
    i_moon.classList.remove("fa-solid", "fa-lightbulb");
    // añadir la clase oscura para mostrar el icono
    i_moon.classList.add("fa-regular", "fa-lightbulb");
}

darkModeToggle.addEventListener("click", () => {
    darkMode = localStorage.getItem('darkMode');
    if (darkMode !== 'enabled') {
        enableDarkMode();
    } else {
        disableDarkMode();
    }
});
